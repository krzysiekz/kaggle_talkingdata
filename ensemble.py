import numpy as np
import pandas as pd

print("Reading the data...\n")
df1 = pd.read_csv('results/light_gbm_2018_05_12_18_35_59/sub_2018_05_12_18_35_59.csv')
df2 = pd.read_csv('results/nn_2018_05_11_08_58_08/sub_2018_05_11_08_58_08.csv')

models = {
    'df1': {
        'name': 'lightgbm',
        'score': 0.9776359,
        'df': df1
    },
    'df2': {
        'name': 'nn',
        'score': 0.9783126,
        'df': df2
    }
}

count_models = len(models)

isa_lg = 0
isa_hm = 0
isa_am = 0
isa_gm = 0
print("Blending...\n")
for df in models.keys():
    isa_lg += np.log(models[df]['df'].is_attributed)
    isa_hm += 1 / models[df]['df'].is_attributed
    isa_am += isa_am
    isa_gm *= isa_gm
isa_lg = np.exp(isa_lg / count_models)
isa_hm = count_models / isa_hm
isa_am = isa_am / count_models
isa_gm = isa_gm ** (1 / count_models)

print("Isa log\n")
print(isa_lg[:count_models])
print()
print("Isa harmo\n")
print(isa_hm[:count_models])

sub_log = pd.DataFrame()
sub_log['click_id'] = df1['click_id']
sub_log['is_attributed'] = isa_lg
sub_log.head()

sub_hm = pd.DataFrame()
sub_hm['click_id'] = df1['click_id']
sub_hm['is_attributed'] = isa_hm
sub_hm.head()

sub_fin = pd.DataFrame()
sub_fin['click_id'] = df1['click_id']
sub_fin['is_attributed'] = (7 * isa_lg + 3 * isa_hm) / 10

print("Writing...")
sub_fin.to_csv('submission_ensemble.csv', index=False, float_format='%.9f')
