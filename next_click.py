import gc
import time
import numpy as np
import pandas as pd

dtype = {
    'ip': np.int32,
    'app': np.int16,
    'device': np.int16,
    'os': np.int16,
    'click_time': object,
}
print("Loading data")
df = pd.read_csv('data/train.csv', dtype=dtype, usecols=dtype.keys(), parse_dates=['click_time'])
df_test = pd.read_csv('data/test.csv', dtype=dtype, usecols=dtype.keys(), parse_dates=['click_time'])

num_train = df.shape[0]

print("Concatenating data")
concat_df = pd.concat([df, df_test])

del df, df_test
gc.collect()

print("Calculating next clicks")
start = time.time()
# click time in seconds
concat_df['click_time'] = (concat_df['click_time'].astype(np.int64) // 10 ** 9).astype(np.int32)
concat_df['nextClick'] = (
        concat_df.groupby(['ip', 'app', 'device', 'os']).click_time.shift(-1).fillna(3000000000) - concat_df.click_time).astype(
    np.int32)

concat_df.drop(['ip', 'app', 'device', 'os', 'click_time'], axis=1, inplace=True)

train = concat_df.iloc[:num_train]
test = concat_df.iloc[num_train:]

print("Saving results")
train.to_csv("data/train_next_click.csv")
test.to_csv("data/test_next_click.csv")

print('Elapsed: {} seconds'.format(time.time() - start))
