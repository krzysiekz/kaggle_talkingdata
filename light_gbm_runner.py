import gc
import time

import lightgbm as lgb
import matplotlib.pyplot as plt
import pandas as pd


class LightGBMRunner:
    def __init__(self, results_directory, data_directory, current_datetime) -> None:
        super().__init__()
        self.method = 'light_gbm'
        self.results_directory = results_directory
        self.data_directory = data_directory
        self.current_datetime = current_datetime

    def get_method(self):
        return self.method

    def run(self, current_run_path):

        # use_validation_set = True
        use_validation_set = False

        dtypes = {
            'ip': 'uint16',
            'app': 'uint16',
            'device': 'uint16',
            'os': 'uint16',
            'channel': 'uint16',
            'is_attributed': 'uint8',
            'click_id': 'uint32',
            'month': 'uint8',
            'day': 'uint8',
            'hour': 'uint8',
            'min': 'uint8',
            'sec': 'uint8',
            'day_of_week': 'uint8',
            'ip_count': 'uint32',
            'ip_app_count': 'uint32',
            'ip_device_count': 'uint32',
            'ip_os_count': 'uint32',
            'ip_channel_count': 'uint32',
            'ip_app_device_count': 'uint32',
            'ip_app_os_count': 'uint32',
            'ip_app_channel_count': 'uint32',
            'ip_device_os_count': 'uint32',
            'ip_device_channel_count': 'uint32',
            'ip_os_channel_count': 'uint32',
            'ip_app_device_os_count': 'uint32',
            'ip_app_device_channel_count': 'uint32',
            'ip_device_os_channel_count': 'uint32',
            'ip_app_device_os_channel_count': 'uint32',
            'ip_count_day_hour': 'uint8',
            'ip_app_count_day_hour': 'uint8',
            'ip_device_count_day_hour': 'uint8',
            'ip_os_count_day_hour': 'uint8',
            'ip_channel_count_day_hour': 'uint8',
            'ip_app_device_count_day_hour': 'uint8',
            'attributed_ip_app_os_count': 'uint8',
            'attributed_ip_count_day_hour': 'uint8',
            'attributed_ip_app_count_day_hour': 'uint8',
            'attributed_ip_device_count_day_hour': 'uint8',
            'attributed_ip_os_count_day_hour': 'uint8',
            'attributed_ip_channel_count_day_hour': 'uint8',
            'attributed_ip_app_device_count_day_hour': 'uint8',
            'nextClick': 'uint8',
        }

        usecols = [
            "app",
            "device",
            "os",
            "channel",
            "day",
            "hour",
            "min",
            "sec",
            "day_of_week",
            "ip_count_day_hour",
            "ip_channel_count_day_hour",
            "ip_os_count_day_hour",
            "ip_device_count_day_hour",
            "ip_app_count_day_hour",
            "ip_app_device_count_day_hour",
            "ip_app_os_count",
            "attributed_ip_app_os_count",
            "attributed_ip_count_day_hour",
            "attributed_ip_app_count_day_hour",
            "attributed_ip_device_count_day_hour",
            "attributed_ip_os_count_day_hour",
            "attributed_ip_channel_count_day_hour",
            "attributed_ip_app_device_count_day_hour",
            "nextClick",
            'is_attributed']

        print('Loading train data...')

        train_df = pd.read_csv(self.data_directory + "train_all11_log2_next_click_resample.csv", usecols=usecols,
                               nrows=41_000_000)
        # skiprows=range(1, 136_903_891), nrows=26_000_000)

        gc.collect()

        len_train = len(train_df)

        if use_validation_set:
            valid_size = int(len_train * 0.2)

            val_df = train_df[(len_train - valid_size):len_train]
            train_df = train_df[:(len_train - valid_size)]
            print("Train size: {}".format(len(train_df)))
            print("Valid size: {}".format(len(val_df)))
        else:
            print("Train size: {}".format(len(train_df)))

        target = 'is_attributed'
        predictors = [
            "app",
            "device",
            "os",
            "channel",
            "day",
            "hour",
            "min",
            "sec",
            "day_of_week",
            "ip_count_day_hour",
            "ip_channel_count_day_hour",
            "ip_os_count_day_hour",
            "ip_device_count_day_hour",
            "ip_app_count_day_hour",
            "ip_app_device_count_day_hour",
            "ip_app_os_count",
            "attributed_ip_app_os_count",
            "attributed_ip_count_day_hour",
            "attributed_ip_app_count_day_hour",
            "attributed_ip_device_count_day_hour",
            "attributed_ip_os_count_day_hour",
            "attributed_ip_channel_count_day_hour",
            "attributed_ip_app_device_count_day_hour",
            "nextClick"
        ]

        categorical = [
            "app",
            "device",
            "os",
            "channel",
            "day",
            "hour",
            "min",
            "sec",
            "day_of_week",
        ]

        print("Predictors: {}.".format(predictors))
        print("Categorical: {}.".format(categorical))

        gc.collect()

        print("Training...")
        start_time = time.time()

        params = {
            'learning_rate': 0.15,
            # 'is_unbalance': 'true', # replaced with scale_pos_weight argument
            'num_leaves': 15,  # 2^max_depth - 1
            'max_depth': 4,  # -1 means no limit
            'min_child_samples': 100,  # Minimum number of data need in a child(min_data_in_leaf)
            'max_bin': 100,  # Number of bucketed bin for feature values
            'subsample': .7,  # Subsample ratio of the training instance.
            'subsample_freq': 1,  # frequence of subsample, <=0 means no enable
            'colsample_bytree': 0.7,  # Subsample ratio of columns when constructing each tree.
            'min_child_weight': 0,  # Minimum sum of instance weight(hessian) needed in a child(leaf)
            'scale_pos_weight': 99  # because training data is extremely unbalanced
        }
        if use_validation_set:
            bst = self.lgb_modelfit_nocv(params,
                                         train_df,
                                         predictors,
                                         dvalid=val_df,
                                         target=target,
                                         objective='binary',
                                         metrics='auc',
                                         early_stopping_rounds=50,
                                         num_boost_round=180,
                                         categorical_features=categorical)
        else:
            bst = self.lgb_modelfit_nocv(params,
                                         train_df,
                                         predictors,
                                         target=target,
                                         objective='binary',
                                         metrics='auc',
                                         early_stopping_rounds=50,
                                         num_boost_round=180,
                                         categorical_features=categorical)

        print('[{}]: model training time'.format(time.time() - start_time))
        del train_df

        if use_validation_set:
            del val_df

        gc.collect()

        print('Saving feature importance...')
        fig, ax = plt.subplots(1, 1, figsize=(20, 10))
        lgb.plot_importance(bst, ax=ax)
        fig.savefig('{}/feature_importance_{}.png'.format(current_run_path, self.current_datetime))

        print('Loading test data...')
        test_df = pd.read_csv(self.data_directory + "test_all11_log2_next_click.csv", dtype=dtypes,
                              usecols=usecols[:-1] + ['click_id'])

        print("Test size : {}".format(len(test_df)))

        sub = pd.DataFrame()
        sub['click_id'] = test_df['click_id'].astype('int')

        print("Predicting...")
        sub['is_attributed'] = bst.predict(test_df[predictors])
        print("Writing...")

        sub.to_csv('{}/sub_{}.csv.gz'.format(current_run_path, self.current_datetime), index=False, compression='gzip')
        print("Done. File written to {}.".format('{}/sub_{}.csv'.format(current_run_path, self.current_datetime)))

    def lgb_modelfit_nocv(self, params, dtrain, predictors, dvalid=None, target='target', objective='binary',
                          metrics='auc',
                          feval=None, early_stopping_rounds=20, num_boost_round=300, categorical_features=None):
        lgb_params = {
            'boosting_type': 'gbdt',
            'objective': objective,
            'metric': metrics,
            'learning_rate': 0.01,
            # 'is_unbalance': 'true',  #because training data is unbalance (replaced with scale_pos_weight)
            'num_leaves': 31,  # we should let it be smaller than 2^(max_depth)
            'max_depth': -1,  # -1 means no limit
            'min_child_samples': 20,  # Minimum number of data need in a child(min_data_in_leaf)
            'max_bin': 255,  # Number of bucketed bin for feature values
            'subsample': 0.6,  # Subsample ratio of the training instance.
            'subsample_freq': 0,  # frequence of subsample, <=0 means no enable
            'colsample_bytree': 0.3,  # Subsample ratio of columns when constructing each tree.
            'min_child_weight': 5,  # Minimum sum of instance weight(hessian) needed in a child(leaf)
            'subsample_for_bin': 200000,  # Number of samples for constructing bin
            'min_split_gain': 0,  # lambda_l1, lambda_l2 and min_gain_to_split to regularization
            'reg_alpha': 0,  # L1 regularization term on weights
            'reg_lambda': 0,  # L2 regularization term on weights
            'nthread': 8,
            'verbose': 0,
        }

        lgb_params.update(params)

        xgtrain = lgb.Dataset(dtrain[predictors].values, label=dtrain[target].values,
                              feature_name=predictors,
                              categorical_feature=categorical_features
                              )

        if dvalid is not None:
            print("Preparing validation data sets.")
            xgvalid = lgb.Dataset(dvalid[predictors].values, label=dvalid[target].values,
                                  feature_name=predictors,
                                  categorical_feature=categorical_features
                                  )

        evals_results = {}

        if dvalid is not None:
            bst1 = lgb.train(lgb_params,
                             xgtrain,
                             valid_sets=[xgtrain, xgvalid],
                             valid_names=['train', 'valid'],
                             evals_result=evals_results,
                             num_boost_round=num_boost_round,
                             early_stopping_rounds=early_stopping_rounds,
                             verbose_eval=10,
                             feval=feval)
        else:
            bst1 = lgb.train(lgb_params,
                             xgtrain,
                             valid_sets=[xgtrain],
                             valid_names=['train'],
                             evals_result=evals_results,
                             num_boost_round=num_boost_round,
                             early_stopping_rounds=early_stopping_rounds,
                             verbose_eval=10,
                             feval=feval)

        n_estimators = bst1.best_iteration
        print("Model Report:")
        print("n_estimators : {}".format(n_estimators))
        if dvalid is not None:
            print("{}:{}".format(metrics, evals_results['valid'][metrics][n_estimators - 1]))
        else:
            print("{}:{}".format(metrics, evals_results['train'][metrics][n_estimators - 1]))

        return bst1
