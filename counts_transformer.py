import argparse
import math


def find_indexes(columns, file):
    header = ""
    indexes = list()
    for line in open(file):
        header = line.rstrip()
        break

    for index, col in enumerate(header.split(",")):
        if col in columns:
            indexes.append(index)

    return indexes


def run(input_file, output, columns):
    counter = 1
    header_processed = False

    columns_as_list = columns.split(",")

    indexes = find_indexes(columns_as_list, input_file)
    print("Indexes: {}".format(indexes))

    if len(indexes) != len(columns_as_list):
        print("Cannot find all indexes for columns")
        return

    with open(output, "w") as out:
        for line in open(input_file):
            if not header_processed:
                out.write(line)
                header_processed = True
                continue
            else:
                line = line.rstrip()
                column_values = line.split(",")
                for index in indexes:
                    current_value = int(column_values[index])
                    new_value = str(int(math.log2(current_value + 1)))
                    column_values[index] = new_value
                out.write(",".join(column_values) + "\n")

            if counter % print_progress_interval == 0:
                print("Processed {} lines".format(counter))
            counter = counter + 1


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Transform count columns to log10 bins.")
    parser.add_argument("file", metavar='file', type=str, help="Input file")
    parser.add_argument("output", metavar='output', type=str, help="Output file.")
    parser.add_argument("columns", metavar='columns', type=str,
                        help="Comma separated column that should be transformed.")

    print_progress_interval = 10_000_000

    args = parser.parse_args()

    run(args.file, args.output, args.columns)
