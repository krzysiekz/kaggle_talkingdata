import datetime
import os
import sys

import light_gbm_runner
from nn_runner import NNRunner

data_directory = "data/"
results_directory = "results"
method = "light_gbm"


def run_light_gbm():
    light_gbm = light_gbm_runner.LightGBMRunner(results_directory, data_directory, current_datetime)
    current_run_path = "{}/{}_{}".format(results_directory, light_gbm.get_method(), current_datetime)
    if not os.path.exists(current_run_path):
        os.makedirs(current_run_path)
    with open('{}/log.txt'.format(current_run_path), 'w', buffering=1) as log_file:
        sys.stdout = log_file
        print("Using directory {}.".format(current_run_path))
        light_gbm.run(current_run_path)


def run_nn():
    nn = NNRunner(results_directory, data_directory, current_datetime)
    current_run_path = "{}/{}_{}".format(results_directory, nn.get_method(), current_datetime)
    if not os.path.exists(current_run_path):
        os.makedirs(current_run_path)
    with open('{}/log.txt'.format(current_run_path), 'w', buffering=1) as log_file:
        sys.stdout = log_file
        print("Using directory {}.".format(current_run_path))
        nn.run(current_run_path)


if __name__ == "__main__":
    current_datetime = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

    run_light_gbm()
    # run_nn()