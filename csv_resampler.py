import argparse


def run(input_file, output, interval):
    counter = 1
    header_processed = False
    with open(output, "w") as out:
        for line in open(input_file):
            if not header_processed:
                out.write(line)
                header_processed = True
                continue
            else:
                if counter % interval == 0:
                    out.write(line)
                    counter = 1
                else:
                    counter = counter + 1


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Re-sample csv file.")
    parser.add_argument("file", metavar='file', type=str, help="Input file")
    parser.add_argument("output", metavar='output', type=str, help="Output file.")
    parser.add_argument("interval", metavar='interval', type=str,
                        help="Interval specified what lines to keep (every n'th line will be kept).")

    args = parser.parse_args()

    run(args.file, args.output, int(args.interval))
