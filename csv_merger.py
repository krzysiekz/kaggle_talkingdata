import argparse
from operator import itemgetter


def find_indexes(columns, second):
    header = ""
    indexes = list()
    for line in open(second):
        header = line.rstrip()
        break

    for index, col in enumerate(header.split(",")):
        if col in columns:
            indexes.append(index)

    return indexes


def merge(first, second, output, columns):
    print("Merging columns {} from file {} to file {} and saving as {}.".format(columns, second, first, output))
    indexes = find_indexes(columns, second)
    if len(indexes) != len(columns):
        print("Cannot find all indexes for columns")
        return

    print("Column indexes {}".format(indexes))
    processed = 1
    with open(first) as first:
        with open(second) as second:
            with open(output, "w") as out:
                while True:
                    try:
                        first_line = first.__next__().rstrip()
                        second_line = second.__next__().rstrip().split(",")
                        if len(indexes) == 1:
                            selected_columns = [itemgetter(*indexes)(second_line)]
                        else:
                            selected_columns = itemgetter(*indexes)(second_line)
                        output_line = "{},{}\n".format(first_line, ",".join(selected_columns))
                        out.write(output_line)
                        if processed % print_process_after == 0:
                            print("Processed {} lines".format(processed))
                        processed = processed + 1
                    except StopIteration:
                        break
    print("Output saved to file {}".format(output))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Add columns form csv file to another.")
    parser.add_argument("first", metavar='first', type=str,
                        help="First file, column from second file will be appended to this one.")
    parser.add_argument("second", metavar='second', type=str, help="Second file with columns that should be appended.")
    parser.add_argument("columns", metavar='columns', type=str, help="Comma separated column to add to input file.")
    parser.add_argument("output", metavar='output', type=str, help="Output file.")

    args = parser.parse_args()

    print_process_after = 10_000_000

    merge(args.first, args.second, args.output, args.columns.split(","))
