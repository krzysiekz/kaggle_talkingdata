import gc

import numpy
import pandas as pd
import tensorflow
from keras import Input, Model
from keras import metrics
from keras.callbacks import Callback, ModelCheckpoint
from keras.layers import Dense, Dropout, Embedding, concatenate, Flatten, BatchNormalization, SpatialDropout1D, Conv1D, \
    K, MaxPooling1D
from keras.optimizers import Adam
from keras.utils import plot_model
from keras.models import load_model
from sklearn.metrics import roc_auc_score


class NNRunner:

    def __init__(self, results_directory, data_directory, current_datetime) -> None:
        super().__init__()
        self.method = 'nn'
        self.results_directory = results_directory
        self.data_directory = data_directory
        self.current_datetime = current_datetime

    def get_method(self):
        return self.method

    def run(self, current_run_path):
        dtypes = {
            'ip': 'uint16',
            'app': 'uint16',
            'device': 'uint16',
            'os': 'uint16',
            'channel': 'uint16',
            'is_attributed': 'uint8',
            'click_id': 'uint32',
            'month': 'uint8',
            'day': 'uint8',
            'hour': 'uint8',
            'min': 'uint8',
            'sec': 'uint8',
            'day_of_week': 'uint8',
            'ip_count': 'uint32',
            'ip_app_count': 'uint32',
            'ip_device_count': 'uint32',
            'ip_os_count': 'uint32',
            'ip_channel_count': 'uint32',
            'ip_app_device_count': 'uint32',
            'ip_app_os_count': 'uint32',
            'ip_app_channel_count': 'uint32',
            'ip_device_os_count': 'uint32',
            'ip_device_channel_count': 'uint32',
            'ip_os_channel_count': 'uint32',
            'ip_app_device_os_count': 'uint32',
            'ip_app_device_channel_count': 'uint32',
            'ip_device_os_channel_count': 'uint32',
            'ip_app_device_os_channel_count': 'uint32',
            'ip_count_day_hour': 'uint16',
            'ip_app_count_day_hour': 'uint16',
            'ip_device_count_day_hour': 'uint16',
            'ip_os_count_day_hour': 'uint16',
            'ip_channel_count_day_hour': 'uint16',
            'ip_app_device_count_day_hour': 'uint16',
            'attributed_ip_app_os_count': 'uint8',
            'attributed_ip_count_day_hour': 'uint8',
            'attributed_ip_app_count_day_hour': 'uint8',
            'attributed_ip_device_count_day_hour': 'uint8',
            'attributed_ip_os_count_day_hour': 'uint8',
            'attributed_ip_channel_count_day_hour': 'uint8',
            'attributed_ip_app_device_count_day_hour': 'uint8',
            'nextClick': 'uint8',
        }

        usecols = [
            # 'ip',
            'app',
            'device',
            'os',
            'channel',
            # 'click_time',
            'month',
            'day',
            'hour',
            'min',
            'sec',
            'day_of_week',
            'ip_count',
            'ip_app_count',
            'ip_device_count',
            'ip_os_count',
            'ip_channel_count',
            'ip_app_device_count',
            'ip_app_os_count',
            'ip_app_channel_count',
            'ip_device_os_count',
            'ip_device_channel_count',
            'ip_os_channel_count',
            'ip_app_device_os_count',
            'ip_app_device_channel_count',
            'ip_device_os_channel_count',
            'ip_app_device_os_channel_count',
            'ip_count_day_hour',
            'ip_app_count_day_hour',
            'ip_device_count_day_hour',
            'ip_device_count_day_hour',
            'ip_os_count_day_hour',
            'ip_channel_count_day_hour',
            'ip_app_device_count_day_hour',
            'attributed_ip_app_os_count',
            'attributed_ip_count_day_hour',
            'attributed_ip_app_count_day_hour',
            'attributed_ip_device_count_day_hour',
            'attributed_ip_os_count_day_hour',
            'attributed_ip_channel_count_day_hour',
            'attributed_ip_app_device_count_day_hour',
            'nextClick',
            'is_attributed']

        use_validation = False

        if use_validation:
            generator = DataIterator("Train", self.data_directory + "train_all11_log2_next_click_train.csv", dtypes,
                                     usecols)
            generator_valid = DataIterator("Valid", self.data_directory + "train_all11_log2_next_click_valid.csv",
                                           dtypes, usecols)
        else:
            generator = DataIterator("Train", self.data_directory + "train_all11_log2_next_click.csv", dtypes, usecols)

        # use_existing_model = None
        use_existing_model = "results/nn_2018_05_11_08_58_08/weights.01.hdf5"
        epochs = 1

        if not use_existing_model:
            print("Preparing Keras model...")

            n_embeddings = 50

            print("Number of embeddings {}".format(n_embeddings))

            app = Input(shape=[1], name="app")
            device = Input(shape=[1], name="device")
            os = Input(shape=[1], name="os")
            channel = Input(shape=[1], name="channel")
            month = Input(shape=[1], name="month")
            day = Input(shape=[1], name="day")
            hour = Input(shape=[1], name="hour")
            min = Input(shape=[1], name="min")
            sec = Input(shape=[1], name="sec")
            day_of_week = Input(shape=[1], name="day_of_week")
            ip_count_day_hour = Input(shape=[1], name="ip_count_day_hour")
            ip_channel_count_day_hour = Input(shape=[1], name="ip_channel_count_day_hour")
            ip_os_count_day_hour = Input(shape=[1], name="ip_os_count_day_hour")
            ip_device_count_day_hour = Input(shape=[1], name="ip_device_count_day_hour")
            ip_app_count_day_hour = Input(shape=[1], name="ip_app_count_day_hour")
            ip_app_device_count_day_hour = Input(shape=[1], name="ip_app_device_count_day_hour")
            ip_app_os_count = Input(shape=[1], name="ip_app_os_count")
            attributed_ip_app_os_count = Input(shape=[1], name="attributed_ip_app_os_count")
            attributed_ip_count_day_hour = Input(shape=[1], name="attributed_ip_count_day_hour")
            attributed_ip_app_count_day_hour = Input(shape=[1], name="attributed_ip_app_count_day_hour")
            attributed_ip_device_count_day_hour = Input(shape=[1], name="attributed_ip_device_count_day_hour")
            attributed_ip_os_count_day_hour = Input(shape=[1], name="attributed_ip_os_count_day_hour")
            attributed_ip_channel_count_day_hour = Input(shape=[1], name="attributed_ip_channel_count_day_hour")
            attributed_ip_app_device_count_day_hour = Input(shape=[1], name="attributed_ip_app_device_count_day_hour")
            next_click = Input(shape=[1], name="next_click")
            # counts = Input(shape=[22], name="counts")

            emb_app = Embedding(706 + 1, n_embeddings)(app)
            emb_device = Embedding(3475 + 1, n_embeddings)(device)
            emb_os = Embedding(800 + 1, n_embeddings)(os)
            emb_channel = Embedding(202 + 1, n_embeddings)(channel)
            # emb_month = Embedding(12 + 1, n_embeddings)(month)
            emb_day = Embedding(31 + 1, n_embeddings)(day)
            emb_hour = Embedding(24 + 1, n_embeddings)(hour)
            emb_min = Embedding(60 + 1, n_embeddings)(min)
            emb_sec = Embedding(60 + 1, n_embeddings)(sec)
            emb_day_of_week = Embedding(7 + 1, n_embeddings)(day_of_week)
            emb_ip_count_day_hour = Embedding(15 + 1, n_embeddings)(ip_count_day_hour)
            emb_ip_channel_count_day_hour = Embedding(11 + 1, n_embeddings)(ip_channel_count_day_hour)
            emb_ip_os_count_day_hour = Embedding(13 + 1, n_embeddings)(ip_os_count_day_hour)
            emb_ip_device_count_day_hour = Embedding(15 + 1, n_embeddings)(ip_device_count_day_hour)
            emb_ip_app_count_day_hour = Embedding(12 + 1, n_embeddings)(ip_app_count_day_hour)
            emb_ip_app_device_count_day_hour = Embedding(12 + 1, n_embeddings)(ip_app_device_count_day_hour)
            emb_ip_app_os_count = Embedding(15 + 1, n_embeddings)(ip_app_os_count)
            emb_attributed_ip_app_os_count = Embedding(8 + 1, n_embeddings)(attributed_ip_app_os_count)
            emb_attributed_ip_count_day_hour = Embedding(7 + 1, n_embeddings)(attributed_ip_count_day_hour)
            emb_attributed_ip_app_count_day_hour = Embedding(5 + 1, n_embeddings)(attributed_ip_app_count_day_hour)
            emb_attributed_ip_device_count_day_hour = Embedding(6 + 1, n_embeddings)(attributed_ip_device_count_day_hour)
            emb_attributed_ip_os_count_day_hour = Embedding(4 + 1, n_embeddings)(attributed_ip_os_count_day_hour)
            emb_attributed_ip_channel_count_day_hour = Embedding(5 + 1, n_embeddings)(attributed_ip_channel_count_day_hour)
            emb_attributed_ip_app_device_count_day_hour = Embedding(5 + 1, n_embeddings)(
                attributed_ip_app_device_count_day_hour)
            emb_next_click = Embedding(30 + 1, n_embeddings)(next_click)

            main = concatenate([
                emb_device,
                emb_os,
                emb_app,
                emb_channel,
                # emb_month,
                emb_day,
                emb_hour,
                emb_min,
                emb_sec,
                emb_day_of_week,
                emb_ip_count_day_hour,
                emb_ip_channel_count_day_hour,
                emb_ip_os_count_day_hour,
                emb_ip_device_count_day_hour,
                emb_ip_app_count_day_hour,
                emb_ip_app_device_count_day_hour,
                emb_ip_app_os_count,
                emb_attributed_ip_app_os_count,
                emb_attributed_ip_count_day_hour,
                emb_attributed_ip_app_count_day_hour,
                emb_attributed_ip_device_count_day_hour,
                emb_attributed_ip_os_count_day_hour,
                emb_attributed_ip_channel_count_day_hour,
                emb_attributed_ip_app_device_count_day_hour,
                emb_next_click
            ])

            dropout = 0.4

            main = SpatialDropout1D(dropout)(main)

            flat = Flatten()(main)

            conv = Conv1D(100, kernel_size=4, padding='same', activation='relu')(main)
            mp = MaxPooling1D(pool_size=4, strides=1, padding='same')(conv)
            flat_2 = Flatten()(mp)

            conv2 = Conv1D(200, kernel_size=4, padding='same', activation='relu')(mp)
            mp2 = MaxPooling1D(pool_size=4, strides=1, padding='same')(conv2)
            flat_3 = Flatten()(mp2)

            concat = concatenate([flat, flat_2, flat_3])

            main = Dense(1200, activation="relu")(concat)
            main = BatchNormalization()(main)
            main = Dropout(dropout)(main)
            main = Dense(1000, activation="relu")(main)
            main = Dropout(dropout)(main)
            main = Dense(500, activation="relu")(main)
            main = Dropout(dropout)(main)

            output = Dense(1, activation="sigmoid")(main)

            model = Model(
                [app, device, os, channel, day, hour, min, sec, day_of_week, ip_count_day_hour, ip_channel_count_day_hour,
                 ip_os_count_day_hour, ip_device_count_day_hour, ip_app_count_day_hour, ip_app_device_count_day_hour,
                 ip_app_os_count, attributed_ip_app_os_count, attributed_ip_count_day_hour,
                 attributed_ip_app_count_day_hour, attributed_ip_device_count_day_hour, attributed_ip_os_count_day_hour,
                 attributed_ip_channel_count_day_hour, attributed_ip_app_device_count_day_hour, next_click],
                output)

            print("number od epochs: {}".format(epochs))

            exp_decay = lambda init, fin, steps: (init / fin) ** (1 / (steps - 1)) - 1
            steps_all_epochs = generator.steps_per_epoch * epochs
            lr_init, lr_fin = 0.0007, 0.0001
            lr_decay = exp_decay(lr_init, lr_fin, steps_all_epochs)

            optimizer_adam = Adam(lr=0.001, decay=lr_decay)
            model.compile(loss='binary_crossentropy', optimizer=optimizer_adam,
                          metrics=[metrics.binary_accuracy, auc_roc])

            print("Saving model summary...")
            print(model.summary())
            plot_model(model, show_shapes=True,
                       to_file='{}/model_summary_{}.png'.format(current_run_path, self.current_datetime))
        else:
            print("Using existing model {}".format(use_existing_model))
            model = load_model(use_existing_model, custom_objects={'auc_roc': auc_roc})

        model_path = "{}/{}".format(current_run_path, "weights.{epoch:02d}.hdf5")

        # if use_validation:
        #     checkpoint = ModelCheckpoint(model_path, monitor='val_auc_roc', verbose=1, save_best_only=True, mode='max')
        # else:
        #     checkpoint = ModelCheckpoint(model_path, monitor='auc_roc', verbose=1, save_best_only=True, mode='max')

        checkpoint = ModelCheckpoint(model_path, monitor='binary_accuracy', verbose=0, save_best_only=False,
                                     save_weights_only=False, mode='auto', period=1)

        out_batch = NBatchLogger(display=10)

        # validation_y_value = pd.read_csv(self.data_directory + "train_all11_sample_valid.csv", dtype=generator.dtypes,
        #                                  usecols=['is_attributed']).as_matrix(['is_attributed'])
        #
        # interval_evaluation = IntervalEvaluation(
        #     DataIterator("InternalValidation", self.data_directory + "train_all11_sample_valid.csv", dtypes, usecols,
        #                  test=True), validation_y_value, interval=100)

        class_weight = {0: .01, 1: .99}

        if use_validation:
            model.fit_generator(generator, steps_per_epoch=generator.steps_per_epoch, epochs=epochs,
                                validation_data=generator_valid, validation_steps=generator_valid.steps_per_epoch,
                                class_weight=class_weight,
                                callbacks=[
                                    out_batch,
                                    checkpoint,
                                    # interval_evaluation
                                ],
                                verbose=2)
        else:
            model.fit_generator(generator, steps_per_epoch=generator.steps_per_epoch, epochs=epochs,
                                class_weight=class_weight,
                                callbacks=[
                                    out_batch,
                                    checkpoint,
                                    # interval_evaluation
                                ],
                                verbose=2)

        generator_test = DataIterator("Test", self.data_directory + "test_all11_log2_next_click.csv", dtypes,
                                      usecols[:-1],
                                      test=True)

        print('Loading test data...')
        test_df = pd.read_csv(self.data_directory + "test_all11_log2_next_click.csv", dtype=generator.dtypes,
                              usecols=['click_id'])

        sub = pd.DataFrame()
        sub['click_id'] = test_df['click_id'].astype('int')

        print("Predicting...")
        sub['is_attributed'] = model.predict_generator(generator_test,
                                                       steps=generator_test.steps_per_epoch, verbose=2)
        print("Writing...")

        sub.to_csv('{}/sub_{}.csv.gz'.format(current_run_path, self.current_datetime), index=False, compression='gzip')
        print("Done. File written to {}.".format('{}/sub_{}.csv.gz'.format(current_run_path, self.current_datetime)))


class DataIterator:

    def __init__(self, name, file_name, dtypes, columns_to_use, batch_size=20_000, test=False):
        self.dtypes = dtypes
        self.columns_to_use = columns_to_use
        self.test = test
        self.name = name

        number_of_samples = sum(1 for line in open(file_name)) - 1
        print("[{}] Number of samples: {}".format(name, number_of_samples))
        gc.collect()

        print("[{}] Batch size: {}".format(name, batch_size))
        self.batch_size = batch_size

        self.steps_per_epoch = int(numpy.ceil(number_of_samples / batch_size))
        print("[{}] Steps per epoch: {}".format(name, self.steps_per_epoch))

        self.file_name = file_name
        self.data = pd.read_csv(file_name, usecols=self.columns_to_use, dtype=self.dtypes, chunksize=batch_size)
        self.current_step = 1

    def __iter__(self):
        return self

    def __next__(self, *args, **kwargs):
        return self.next()

    def next(self):
        try:
            chunk = self.data.get_chunk()
            self.current_step = self.current_step + 1
        except StopIteration:
            del self.data
            gc.collect()
            self.data = pd.read_csv(self.file_name, usecols=self.columns_to_use, dtype=self.dtypes,
                                    chunksize=self.batch_size)
            chunk = self.data.get_chunk()
            self.current_step = 1
        x = {
            'app': chunk.as_matrix(['app']),
            'device': chunk.as_matrix(['device']),
            'os': chunk.as_matrix(['os']),
            'channel': chunk.as_matrix(['channel']),
            'month': chunk.as_matrix(['month']),
            'day': chunk.as_matrix(['day']),
            'hour': chunk.as_matrix(['hour']),
            'min': chunk.as_matrix(['min']),
            'sec': chunk.as_matrix(['sec']),
            'day_of_week': chunk.as_matrix(['day_of_week']),
            'ip_count_day_hour': chunk.as_matrix(['ip_count_day_hour']),
            'ip_channel_count_day_hour': chunk.as_matrix(['ip_channel_count_day_hour']),
            'ip_os_count_day_hour': chunk.as_matrix(['ip_os_count_day_hour']),
            'ip_device_count_day_hour': chunk.as_matrix(['ip_device_count_day_hour']),
            'ip_app_count_day_hour': chunk.as_matrix(['ip_app_count_day_hour']),
            'ip_app_device_count_day_hour': chunk.as_matrix(['ip_app_device_count_day_hour']),
            'ip_app_os_count': chunk.as_matrix(['ip_app_os_count']),
            'attributed_ip_app_os_count': chunk.as_matrix(['attributed_ip_app_os_count']),
            'attributed_ip_count_day_hour': chunk.as_matrix(['attributed_ip_count_day_hour']),
            'attributed_ip_app_count_day_hour': chunk.as_matrix(['attributed_ip_app_count_day_hour']),
            'attributed_ip_device_count_day_hour': chunk.as_matrix(['attributed_ip_device_count_day_hour']),
            'attributed_ip_os_count_day_hour': chunk.as_matrix(['attributed_ip_os_count_day_hour']),
            'attributed_ip_channel_count_day_hour': chunk.as_matrix(['attributed_ip_channel_count_day_hour']),
            'attributed_ip_app_device_count_day_hour': chunk.as_matrix(['attributed_ip_app_device_count_day_hour']),
            'next_click': chunk.as_matrix(['nextClick']),
            # 'counts': chunk.as_matrix(['ip_count',
            #                            'ip_app_count',
            #                            'ip_device_count',
            #                            'ip_os_count',
            #                            'ip_channel_count',
            #                            'ip_app_device_count',
            #                            'ip_app_os_count',
            #                            'ip_app_channel_count',
            #                            'ip_device_os_count',
            #                            'ip_device_channel_count',
            #                            'ip_os_channel_count',
            #                            'ip_app_device_os_count',
            #                            'ip_app_device_channel_count',
            #                            'ip_device_os_channel_count',
            #                            'ip_app_device_os_channel_count',
            #                            'ip_count_day_hour',
            #                            'ip_app_count_day_hour',
            #                            'ip_device_count_day_hour',
            #                            'ip_device_count_day_hour',
            #                            'ip_os_count_day_hour',
            #                            'ip_channel_count_day_hour',
            #                            'ip_app_device_count_day_hour', ]),
        }
        # print("[{}] Batch step {}/{}".format(self.name, self.current_step, self.steps_per_epoch))
        if not self.test:
            y = chunk.as_matrix(['is_attributed'])
            return x, y
        else:
            return x


class NBatchLogger(Callback):
    """
    A Logger that log average performance per `display` steps.
    """

    def __init__(self, display):
        super().__init__()
        self.step = 0
        self.display = display
        self.metric_cache = {}

    def on_batch_end(self, batch, logs=None):
        if logs is None:
            logs = {}
        self.step += 1
        for k in self.params['metrics']:
            if k in logs:
                self.metric_cache[k] = self.metric_cache.get(k, 0) + logs[k]
        if self.step % self.display == 0:
            metrics_log = ''
            for (k, v) in self.metric_cache.items():
                val = v / self.display
                if abs(val) > 1e-3:
                    metrics_log += ' - %s: %.4f' % (k, val)
                else:
                    metrics_log += ' - %s: %.4e' % (k, val)
            print('step: {}/{} ... {}'.format(self.step,
                                              self.params['steps'],
                                              metrics_log))
            self.metric_cache.clear()


class IntervalEvaluation(Callback):
    def __init__(self, validation_data_generator, y_value, interval=200):
        super(Callback, self).__init__()

        self.interval = interval
        self.validation_data_generator = validation_data_generator
        self.y_value = y_value

    def on_batch_end(self, batch, logs=None):
        if batch > 1 and batch % self.interval == 0:
            y_prediction = self.model.predict_generator(self.validation_data_generator, verbose=0,
                                                        steps=self.validation_data_generator.steps_per_epoch)
            score = roc_auc_score(self.y_value, y_prediction)
            print("Interval evaluation - batch: {:d} - score: {:.6f}".format(batch, score))

    def on_train_end(self, logs=None):
        super().on_train_end(logs)
        y_prediction = self.model.predict_generator(self.validation_data_generator, verbose=0,
                                                    steps=self.validation_data_generator.steps_per_epoch)
        score = roc_auc_score(self.y_value, y_prediction)
        print("Interval evaluation - Final roc auc: {:.6f}".format(score))


# def auc_roc(y_true, y_pred):
#     # any tensorflow metric
#     value, update_op = tensorflow.contrib.metrics.streaming_auc(y_pred, y_true)
#
#     # find all variables created for this metric
#     metric_vars = [i for i in tensorflow.local_variables() if 'auc_roc' in i.name.split('/')[1]]
#
#     # Add metric variables to GLOBAL_VARIABLES collection.
#     # They will be initialized for new session.
#     for v in metric_vars:
#         tensorflow.add_to_collection(tensorflow.GraphKeys.GLOBAL_VARIABLES, v)
#
#     # force to update metric values
#     with tensorflow.control_dependencies([update_op]):
#         value = tensorflow.identity(value)
#         return value


# -----------------------------------------------------------------------------------------------------------------------------------------------------
# AUC for a binary classifier
def auc_roc(y_true, y_pred):
    ptas = tensorflow.stack([binary_PTA(y_true, y_pred, k) for k in numpy.linspace(0, 1, 1000)], axis=0)
    pfas = tensorflow.stack([binary_PFA(y_true, y_pred, k) for k in numpy.linspace(0, 1, 1000)], axis=0)
    pfas = tensorflow.concat([tensorflow.ones((1,)), pfas], axis=0)
    binSizes = -(pfas[1:] - pfas[:-1])
    s = ptas * binSizes
    return K.sum(s, axis=0)


# -----------------------------------------------------------------------------------------------------------------------------------------------------
# PFA, prob false alert for binary classifier
def binary_PFA(y_true, y_pred, threshold=K.variable(value=0.5)):
    y_pred = K.cast(y_pred >= threshold, 'float32')
    # N = total number of negative labels
    N = K.sum(1 - y_true)
    # FP = total number of false alerts, alerts from the negative class labels
    FP = K.sum(y_pred - y_pred * y_true)
    return FP / N


# -----------------------------------------------------------------------------------------------------------------------------------------------------
# P_TA prob true alerts for binary classifier
def binary_PTA(y_true, y_pred, threshold=K.variable(value=0.5)):
    y_pred = K.cast(y_pred >= threshold, 'float32')
    # P = total number of positive labels
    P = K.sum(y_true)
    # TP = total number of correct alerts, alerts from the positive class labels
    TP = K.sum(y_pred * y_true)
    return TP / P
