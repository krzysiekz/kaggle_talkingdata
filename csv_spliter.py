import argparse


def find_column_index(column, header):
    for index, col in enumerate(header.split(",")):
        if column == col:
            return index


def run(input_file, column, expression, first_output, second_output):
    print("Splitting file {} into {} and {} based on column {} ({})".format(input_file, first_output, second_output,
                                                                            column, expression))
    header_processed = False
    column_index = None
    counter = 1
    with open(first_output, "w") as first:
        with open(second_output, "w") as second:
            for line in open(input_file):
                if not header_processed:
                    first.write(line)
                    second.write(line)
                    column_index = find_column_index(column, line)

                    if not column_index:
                        print("Cannot find column {}".format(column))
                        break

                    header_processed = True
                    continue
                else:
                    column_value = line.split(",")[column_index]
                    if expression in column_value:
                        second.write(line)
                    else:
                        first.write(line)

                if counter % print_progress_interval == 0:
                    print("Processed {} lines".format(counter))
                counter = counter + 1
                
    print("Done")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Split CSV file based on column expression")
    parser.add_argument("file", metavar='file', type=str, help="Input file")
    parser.add_argument("column", metavar='column', type=str, help="Column")
    parser.add_argument("expression", metavar='expression', type=str, help="Expression")
    parser.add_argument("first_output", metavar='first_output', type=str,
                        help="First output file.")
    parser.add_argument("second_output", metavar='second_output', type=str,
                        help="Second output file.")

    print_progress_interval = 10_000_000

    args = parser.parse_args()

    run(args.file, args.column, args.expression, args.first_output, args.second_output)
